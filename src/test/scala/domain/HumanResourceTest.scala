package domain

import domain.SimpleTypes.*
import domain.{DomainError, SimpleTypes}
import io.FileIO.load
import org.scalatest.funsuite.AnyFunSuite
import xml.XML.{fromAttribute, traverse}
import domain.PhysicalResource
import domain.HumanResource
import scala.language.adhocExtensions
import scala.xml.{Elem, Node, XML}

class HumanResourceTest extends AnyFunSuite:
  test("testFrom") {
    val file = load("files/dev/ms01/dev_validAgenda_test_in.xml")

    val resultNode = file.flatMap(elemXML => traverse((elemXML \\ "Physical"), (n: Node) => PhysicalResource.from(n)))
    val result = resultNode match
      case Left(a) => null
      case Right(b) => b

    val resultNodeHuman = file.flatMap(elemXML => traverse((elemXML \\ "Human"), (n: Node) => HumanResource.from(n)))
    val resultHuman = resultNodeHuman match
      case Left(a) => null
      case Right(b) => b

    val TrueResult = for {
      id <- STHumanId.from("HRS_1")
      name <- NonEmptyString.from("Antonio")
      handle <- STPhysicalTypeId.from("PRST 1")
    } yield HumanResource(id, name, Seq(handle))

    resultHuman.map(elem => assert(Right(elem) == TrueResult))

  }
