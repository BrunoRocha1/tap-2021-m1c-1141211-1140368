package domain

import io.FileIO.load
import scala.language.adhocExtensions
import org.scalatest.funsuite.AnyFunSuite
import xml.XML.traverse
import xml.XML.fromAttribute
import domain.DomainError
import domain.*
import scala.xml.Node
import domain.SimpleTypes.*
import scala.xml.XML
import scala.xml.Elem

class ProductionTest extends AnyFunSuite:
  test("testFrom") {
    val file = load("files/dev/ms01/dev_validAgenda_test_in.xml")
    val production = file.flatMap(elemXML => Production.from(elemXML))
    val resultProduction = production match
      case Left(a) => null
      case Right(b) => b

    val physicalResource = for{
      id <- STPhysicalId.from("PRS_1")
      tippo <- STPhysicalTypeId.from("PRST 1")
    }yield PhysicalResource(id,tippo)
    val RPr = physicalResource match
      case Left(a) => null
      case Right(b) => b

    val hr = for {
      id <- STHumanId.from("HRS_1")
      name <- NonEmptyString.from("Antonio")
      handle <- STPhysicalTypeId.from("PRST 1")
    } yield HumanResource(id, name, Seq(handle))

    val task = for{
      id <- STTaskId.from("TSK_1")
      time <- Time.from("100")
      physicalResourceType <- STPhysicalTypeId.from("PRST 1")
    }yield Task(id,time,Seq(physicalResourceType))
    val RTask = task match
      case Left(a) => null
      case Right(b) => b

    val product = for{
      id <- STProductId.from("PRD_1")
      time <- NonEmptyString.from("Product 1")
      taskID <- STTaskId.from("TSK_1")
    }yield Product(id,time,Seq(taskID))
    val RProd = product match
      case Left(a) => null
      case Right(b) => b

    val order = for{
      id <- STOrderId.from("ORD_1")
      quantity <- Quantity.from("1")
      prodID <- STProductId.from("PRD_1")
    }yield Order(id,prodID,quantity)
    val ROrd = order match
      case Left(a) => null
      case Right(b) => b

    println(physicalResource.toSeq)
    val TrueResult = Production(physicalResource.toSeq,task.toSeq,hr.toSeq,product.toSeq,order.toSeq)
    assert(resultProduction == TrueResult)
  }

