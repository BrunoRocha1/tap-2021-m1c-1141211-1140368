package domain

import io.FileIO.load
import scala.language.adhocExtensions
import org.scalatest.funsuite.AnyFunSuite
import xml.XML.traverse
import xml.XML.fromAttribute
import domain.DomainError
import domain.*
import domain.TaskSchedule
import scala.xml.Node
import domain.SimpleTypes.*
import scala.xml.XML
import scala.xml.Elem
import domain.Utils.*

class TaskScheduleTest extends AnyFunSuite:
  test("testFrom") {
    val file = load("files/dev/ms01/dev_validAgenda_test_in.xml")
    val resultPhysical = file.flatMap(elemXML => traverse( (elemXML \\ "Physical"), (n: Node) => PhysicalResource.from(n)))
    val resultPR = resultPhysical match
      case Left(a) => null
      case Right(b) => b

    val resultNodeHuman = file.flatMap(elemXML => traverse((elemXML \\ "Human"), (n: Node) => HumanResource.from(n)))
    val resultHuman = resultNodeHuman match
      case Left(a) => null
      case Right(b) => b

    val taskSchedule =
      for
        orderId <- STOrderId.from("ORD_1")
        productNumber <- Quantity.from(1)
        taskId <- STTaskId.from("TSK_1")
        start <- Time.from(0)
        end <- Time.from(0)
      yield TaskSchedule.from(orderId, productNumber, taskId, start, end, resultPR.toSeq.map(_.id), resultHuman.toSeq.map(_.name))

    val output =
      for
        orderId <- STOrderId.from("ORD_1")
        productNumber <- Quantity.from(1)
        taskId <- STTaskId.from("TSK_1")
        start <- Time.from(0)
        end <- Time.from(0)
        pr <- convertSeqResultToResultSeq(Seq(STPhysicalId.from("PRS_1")))
        hr <-  convertSeqResultToResultSeq(Seq(NonEmptyString.from("Antonio")))
      yield TaskSchedule(orderId, productNumber, taskId, start, end, pr, hr)

    val re = taskSchedule match
      case Left(a) => null
      case Right(b) => b

    assert(re == output)
  }


