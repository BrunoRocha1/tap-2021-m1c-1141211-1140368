package domain

import domain.PhysicalResource
import domain.SimpleTypes.*
import domain.{DomainError, SimpleTypes}
import io.FileIO.load
import org.scalatest.funsuite.AnyFunSuite
import xml.XML.{fromAttribute, traverse}

import scala.language.adhocExtensions
import scala.xml.{Elem, Node, XML}

class PhysicalResourceTest extends AnyFunSuite:
  test("testFrom") {
    val file = load("files/dev/ms01/dev_validAgenda_test_in.xml")
    val resultNode = file.flatMap(elemXML => traverse( (elemXML \\ "Physical"), (n: Node) => PhysicalResource.from(n)))
    val result = resultNode match
      case Left(a) => null
      case Right(b) => b
    val TrueResult = for{
      id <- STPhysicalId.from("PRS_1")
      t <- STPhysicalTypeId.from("PRST 1")
    }yield PhysicalResource(id,t)

    result.map(elem => assert(Right(elem) == TrueResult))
  }
