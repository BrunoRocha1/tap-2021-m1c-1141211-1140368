package domain

import domain.SimpleTypes.*
import domain.{DomainError, SimpleTypes}
import io.FileIO.load
import org.scalatest.funsuite.AnyFunSuite
import xml.XML.{fromAttribute, traverse}

import scala.language.adhocExtensions
import scala.xml.{Elem, Node, XML}

class TaskTest extends AnyFunSuite:
  test("testFrom") {
    val file = load("files/dev/ms01/dev_validAgenda_test_in.xml")

    val resultNode = file.flatMap(elemXML => traverse( (elemXML \\ "Physical"), (n: Node) => PhysicalResource.from(n)))
    val result = resultNode match
      case Left(a) => null
      case Right(b) => b

    val resultNodeTask = file.flatMap(elemXML => traverse( (elemXML \\ "Task"), (n: Node) => Task.from(result)(n)))
    val resultTask = resultNodeTask match
      case Left(a) => null
      case Right(b) => b

    val TrueResult = for{
      id <- STTaskId.from("TSK_1")
      time <- Time.from("100")
      physicalResourceType <- STPhysicalTypeId.from("PRST 1")
    }yield Task(id,time,Seq(physicalResourceType))

    resultTask.map(elem => assert(Right(elem) == TrueResult))
  }
