package domain

import io.FileIO.load
import scala.language.adhocExtensions
import org.scalatest.funsuite.AnyFunSuite
import xml.XML.traverse
import xml.XML.fromAttribute
import domain.DomainError
import domain.*
import scala.xml.Node
import domain.SimpleTypes.*
import scala.xml.XML
import scala.xml.Elem

class OrderTest extends AnyFunSuite:
  test("testFrom") {
    val file = load("files/dev/ms01/dev_validAgenda_test_in.xml")

    val resultNode = file.flatMap(elemXML => traverse( (elemXML \\ "Physical"), (n: Node) => PhysicalResource.from(n)))
    val result = resultNode match
      case Left(a) => null
      case Right(b) => b

    val resultNodeTask = file.flatMap(elemXML => traverse( (elemXML \\ "Task"), (n: Node) => Task.from(result)(n)))
    val resultTask = resultNodeTask match
      case Left(a) => null
      case Right(b) => b

    val resultNodeProduct = file.flatMap(elemXML => traverse( (elemXML \\ "Product"), (n: Node) => Product.from(resultTask)(n)))
    val resultProduct = resultNodeProduct match
      case Left(a) => null
      case Right(b) => b

    val resultNodeOrder = file.flatMap(elemXML => traverse( (elemXML \\ "Order"), (n: Node) => Order.from(resultProduct)(n)))
    val resultOrder = resultNodeOrder match
      case Left(a) => null
      case Right(b) => b

    val task = for{
      id <- STTaskId.from("TSK_1")
      time <- Time.from("100")
      physicalResourceTypes <- STPhysicalTypeId.from("PRST 1")
    }yield Task(id,time,Seq(physicalResourceTypes))

    val product = for{
      id <- STProductId.from("PRD_1")
      name <- NonEmptyString.from("Product 1")
      processes <- STTaskId.from("TSK_1")
    }yield Product(id,name, Seq(processes))

    val TrueResult = for{
      id <- STOrderId.from("ORD_1")
      quantity <- Quantity.from("1")
      prodID <- STProductId.from("PRD_1")
    }yield Order(id,prodID,quantity)

    resultOrder.map(elem => assert(Right(elem) == TrueResult))
  }