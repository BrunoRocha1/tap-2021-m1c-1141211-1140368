package domain

import io.FileIO.load
import scala.language.adhocExtensions
import org.scalatest.funsuite.AnyFunSuite
import xml.XML.traverse
import xml.XML.fromAttribute
import domain.DomainError.*
import scala.xml.Node
import domain.SimpleTypes.*
import scala.xml.XML
import scala.xml.Elem

class SimpleTypesTest extends AnyFunSuite:
  test("testSTTaskId") {
    def value = "TSK_1"
    def result = STTaskId.from(value)
    def expected = Right(value)
    assert(result == expected)
  }

  test("testSTTaskIdInvalid") {
    def value = "TASK_1"
    def result = STTaskId.from(value)
    def expected = Left(InvalidTaskId(value))
    assert(result == expected)
  }
  test("testSTPhysicalId") {
    def value = "PRS_1"
    def result = STPhysicalId.from(value)
    def expected = Right(value)
    assert(result == expected)
  }

  test("testSTPhysicalIdInvalid") {
    def value = "Physical_1"
    def result = STPhysicalId.from(value)
    def expected = Left(InvalidPhysicalId(value))
    assert(result == expected)
  }

  test("testSTProductId") {
    def value = "PRD_1"
    def result = STProductId.from(value)
    def expected = Right(value)
    assert(result == expected)
  }

  test("testSTProductIdInvalid") {
    def value = "Product_1"
    def result = STProductId.from(value)
    def expected = Left(InvalidProductId(value))
    assert(result == expected)
  }

  test("testSTHumanId") {
    def value = "HRS_1"
    def result = STHumanId.from(value)
    def expected = Right(value)
    assert(result == expected)
  }

  test("testSTHumanIdInvalid") {
    def value = "HumanResources_1"
    def result = STHumanId.from(value)
    def expected = Left(InvalidHumanId(value))
    assert(result == expected)
  }

  test("testSTOrderId") {
    def value = "ORD_1"
    def result = STOrderId.from(value)
    def expected = Right(value)
    assert(result == expected)
  }

  test("testSTOrderIdInvalid") {
    def value = "Order_1"
    def result = STOrderId.from(value)
    def expected = Left(InvalidOrderId(value))
    assert(result == expected)
  }

  test("testQuantity") {
    def value = 2
    def result = Quantity.from(value)
    def expected = Right(value)
    assert(result == expected)
  }

  test("testQuantityInvalid") {
    def value = -1
    def result = Quantity.from(value)
    def expected = Left(InvalidQuantity(value.toString))
    assert(result == expected)
  }

  test("testTime") {
    def value = 2
    def result = Quantity.from(value)
    def expected = Right(value)
    assert(result == expected)
  }

  test("testTimeInvalid") {
    def value = -1
    def result = Time.from(value)
    def expected = Left(InvalidTime(value.toString))
    assert(result == expected)
  }

  test("testNonEmptyString") {
    def value = "PRD_1"
    def result = NonEmptyString.from(value)
    def expected = Right(value)
    assert(result == expected)
  }

  test("testNonEmptyStringInvalid") {
    def value = ""
    def result = NonEmptyString.from(value)
    def expected = Left(EmptyString(value))
    assert(result == expected)
  }





