package PBT

import domain.*
import domain.SimpleTypes.*
import org.scalacheck.Properties
import org.scalacheck.*
import org.scalacheck.Prop.forAll
import PBT.GenSimpleTypes.*
import PBT.GenUtils.*
import PBT.GenDomain.*

import scala.language.adhocExtensions

object ScheduleServicePropertyTest extends Properties("ScheduleServiceProperties"):
  // schedule service to test
  val ScheduleServiceToTest = ScheduleService

  override def overrideParameters(prms: Test.Parameters): Test.Parameters =
    prms.withMinSuccessfulTests(300)

  property("The same resource cannot be used by the same task") = forAll(genProduction) (prod =>
    val schedule = ScheduleServiceToTest.createSchedule(prod)

    schedule match {
      case Left(s) => true
      case Right(s) => s.forall {
        ts => ts.humanResourceNames.distinct.size == ts.humanResourceNames.size && ts.physicalResourceIds.distinct.size == ts.physicalResourceIds.size
      }
    }
  )

  property("The same resource cannot be used at the same time by two task") = forAll(genProduction) ( prod =>
    val schedule = ScheduleServiceToTest.createSchedule(prod)

    schedule match {
      case Left(s) => true
      case Right(s) =>
        s.forall {
          ts => !s.filter(s1 => s1.order != ts.order && s1.productNumber != ts.productNumber && s1.task != ts.task)
            .exists(s1 => s1.overlaps(ts) && (s1.humanResourceNames.exists(hr => ts.humanResourceNames.contains(hr)) ||
              s1.physicalResourceIds.exists(pr => ts.physicalResourceIds.contains(pr))))
      }
    }
  )

  property("The complete schedule must schedule all the tasks of all the products needed") = forAll(genProduction) ( prod =>
    val schedule = ScheduleServiceToTest.createSchedule(prod)

    val toFulfill = for
      o <- prod.orders
      p <- prod.products.filter(_.id == o.productIdRef)
      q <- 1 to o.quantity.to
      t <- p.processes
    yield (o.orderId, Quantity.from(q).getOrElse(throw Exception()), t)

    schedule match {
      case Left(s) => true
      case Right(s) => toFulfill.forall{
        (o: STOrderId, q: Quantity, t: STTaskId) => s.exists(ts => ts.order == o && ts.productNumber == q && ts.task == t)
      }
    }
  )

  property("All physical resources needed for a task must be allocated to it") = forAll(genProduction) ( prod =>
    val schedule = ScheduleServiceToTest.createSchedule(prod)

    schedule match {
    case Left(s) => true
    case Right(s) =>
      s.forall {
        ts => prod.tasks.filter(_.id == ts.task).headOption.fold(false)(t => t.physicalResourceTypes.forall {
          prt => prod.physicalResources.filter(_.resourceType == prt).exists(pr => ts.physicalResourceIds.contains(pr.id))
        })
      }
    }
  )

  property("Each task must have a human resource to handle each physical resource") = forAll(genProduction) ( prod =>
    val schedule = ScheduleServiceToTest.createSchedule(prod)

    schedule match {
      case Left(s) => true
      case Right(s) =>
        s.forall {
          ts => prod.tasks.filter(_.id == ts.task).headOption.fold(false)(t => t.physicalResourceTypes.forall {
            prt => prod.humanResources.filter(_.handles.contains(prt)).map(_.name).exists(hr => ts.humanResourceNames.contains(hr))
          })
        }
    }
  )

  property("Each task must have one human resource for each physical resource") = forAll(genProduction) ( prod =>
    val schedule = ScheduleServiceToTest.createSchedule(prod)

    schedule match {
      case Left(s) => true
      case Right(s) =>
        s.forall {
          ts => ts.humanResourceNames.size == ts.physicalResourceIds.size
        }
    }
  )