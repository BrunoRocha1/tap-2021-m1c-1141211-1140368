package PBT

import org.scalacheck.Gen

object GenUtils :

  def genList[A](gen: Gen[A], max: Int, min: Int = 1): Gen[List[A]] =
    for
      n <- Gen.choose[Int](min, max)
      l <- Gen.listOfN(n, gen)
    yield l

  def genPick[A](list: List[A], max: Int, min: Int = 1): Gen[Seq[A]] =
    for
      n <- Gen.choose[Int](min, max)
      l <- Gen.pick(n, list)
    yield l.toSeq