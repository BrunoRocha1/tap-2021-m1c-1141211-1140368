package PBT

import org.scalacheck.Gen
import domain.SimpleTypes.*

object GenSimpleTypes:

  val maxGenIdRange = 50
  val minGenRange = 1
  val maxGenTimeRange = 300
  val minGenTimeRange = 50
  val maxGenQuantityRange = 10
  val maxGenNonEmptyStringRange = 5

  def genNonEmptyString: Gen[NonEmptyString] =
    for
      s <- Gen.alphaStr.suchThat(_.size > maxGenNonEmptyStringRange)
    yield NonEmptyString.from(s).getOrElse(throw Exception())

  def genSTHumanId: Gen[STHumanId] =
    for
      id <- Gen.choose[Int](minGenRange,maxGenIdRange)
    yield STHumanId.from("HRS_" + id).getOrElse(throw Exception())

  def genSTOrderId: Gen[STOrderId]=
    for
      id <- Gen.choose[Int](minGenRange,maxGenIdRange)
    yield STOrderId.from("ORD_" + id).getOrElse(throw Exception())

  def genSTQuantity: Gen[Quantity]=
    for
      id <- Gen.choose[Int](minGenRange,maxGenQuantityRange)
    yield Quantity.from(id).getOrElse(throw Exception())

  def genSTTaskId: Gen[STTaskId]=
    for
      id <- Gen.choose[Int](minGenRange,maxGenIdRange)
    yield STTaskId.from("TSK_" + id).getOrElse(throw Exception())

  def genSTTime: Gen[Time]=
    for
      id <- Gen.choose[Int](minGenTimeRange,maxGenTimeRange)
    yield Time.from(id).getOrElse(throw Exception())

  def genSTPhysicalTypeId: Gen[STPhysicalTypeId]=
    for
      id <- Gen.choose[Int](minGenRange,maxGenIdRange)
    yield STPhysicalTypeId.from("PRST " + id).getOrElse(throw Exception())

  def genSTPhysicalId: Gen[STPhysicalId]=
    for
      id <- Gen.choose[Int](minGenRange,maxGenIdRange)
    yield STPhysicalId.from("PRS_ " + id).getOrElse(throw Exception())

  def genSTProductId: Gen[STProductId]=
    for
      id <- Gen.choose[Int](minGenRange,maxGenIdRange)
    yield STProductId.from("PRD_" + id).getOrElse(throw Exception())
