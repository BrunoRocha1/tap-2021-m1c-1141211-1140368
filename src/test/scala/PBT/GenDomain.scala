package PBT

import domain.*
import domain.SimpleTypes.*
import org.scalacheck.*
import PBT.GenSimpleTypes.*
import PBT.GenUtils.*

object GenDomain:

  val maxGenOrderRange = 10
  val maxGenProductRange = 10
  val maxGenResourceRange = 30
  val minGenResourceRange = 10
  val maxGenTaskRange = 10
  val minGenTaskRange = 5
  val maxGenPhysicalTypeRange = 10
  val minGenPhysicalTypeRange = 5
  val maxPhysicalTypesForTask = 5

  def genPhysicalResource(prt: List[STPhysicalTypeId]): Gen[PhysicalResource]=
    for
      id <- genSTPhysicalId
      resourceType <- Gen.oneOf(prt)
    yield PhysicalResource(id, resourceType)

  def genHumanResource(pr: List[PhysicalResource]): Gen[HumanResource]=
    for
      id <- genSTHumanId
      name <- genNonEmptyString
      handles <- Gen.atLeastOne(pr.map(_.resourceType))
    yield HumanResource(id, name, handles.toSeq)

  def genTask(pr: List[PhysicalResource]): Gen[Task]=
    for
      id <- genSTTaskId
      time <- genSTTime
      physicalResourceTypes <- genPick(pr.map(_.resourceType), maxPhysicalTypesForTask)
    yield Task(id, time, physicalResourceTypes.toSeq)

  def genProduct(t: List[Task]): Gen[Product]=
    for
      id <- genSTProductId
      name <- genNonEmptyString
      processes <- Gen.atLeastOne(t.map(_.id))
    yield Product(id, name, processes.toSeq)

  def genOrder(p: List[Product]): Gen[Order]=
    for
      orderId <- genSTOrderId
      productIdRef <- Gen.oneOf(p.map(_.id))
      quantity <- genSTQuantity
    yield Order(orderId, productIdRef, quantity)

  def genProduction: Gen[Production] =
    for
      prt <- genList(genSTPhysicalTypeId, maxGenPhysicalTypeRange, minGenPhysicalTypeRange)
      pr <- genList(genPhysicalResource(prt), maxGenResourceRange, minGenResourceRange)
      t <- genList(genTask(pr.distinctBy(_.id)), maxGenTaskRange, minGenTaskRange)
      hr <- genList(genHumanResource(pr.distinctBy(_.id)), maxGenResourceRange, minGenResourceRange)
      p <- genList(genProduct(t), maxGenProductRange)
      o <- genList(genOrder(p), maxGenOrderRange)
    yield Production(pr.distinctBy(_.id), t.distinctBy(_.id), hr.distinctBy(_.id), p.distinctBy(_.id), o.distinctBy(_.orderId))

