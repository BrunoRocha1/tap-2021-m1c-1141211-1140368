package domain

import domain.Production.*
import domain.DomainError.*
import domain.SimpleTypes.*
import domain.TaskSchedule.*
import domain.ToSchedule.*
import domain.Utils.*

import scala.annotation.tailrec

object OptimizedScheduleServiceV2 extends ScheduleOps:

  def amountOfBlockedTasks(pr: Seq[PhysicalResource], hr: Seq[HumanResource],
                           schedulable: Seq[ToSchedule], t: Time): Int =
    schedulable
      .map(x => optimizedSchedule(pr, hr, t, x, Seq.empty[Task]))
      .filter(_.isLeft)
      .size

  def scheduleFirst(pr: Seq[PhysicalResource], hr: Seq[HumanResource],toSchedule :Seq[ToSchedule],
                      s: Seq[TaskSchedule], t: Time): Option[TaskSchedule] =
    toSchedule
      .map(x => (x,optimizedSchedule(pr, hr, t, x, toSchedule.filterNot(y => y == x).map(_.tasks.head))))
      .collect { case (x,Right(y)) => (x,y)}
      .map(x => (x._1,amountOfBlockedTasks(pr.filterNot(r => x._2.physicalResourceIds.contains(r.id)),
        hr.filterNot(r => x._2.humanResourceNames.contains(r.name)), toSchedule.filterNot(y => x._1 == y), t), x._2))
      .sortBy(x => (x._2,!x._1.hasBeenStarted(s),x._1.order.orderId.to,x._1.productNumber.to))
      .map(_._3)
      .headOption

  def optimizedSchedule(pr: Seq[PhysicalResource], hr: Seq[HumanResource], t: Time, toSchedule :ToSchedule,
                 remainingTasks: Seq[Task]): Result[TaskSchedule] =
    ResourceService.getResources(toSchedule.tasks.head, pr) match
      case Left(a) => Left(a)
      case Right(pr2) => OptimizedResourceService.getResources(toSchedule.tasks.head, hr, remainingTasks) match
        case Left(b) => Left(b)
        case Right(hr2) => TaskSchedule.from(toSchedule.order.orderId, toSchedule.productNumber,
          toSchedule.tasks.head.id, t, t + toSchedule.tasks.head.time,
          pr2.map(_.asInstanceOf[PhysicalResource].id), hr2.map(_.asInstanceOf[HumanResource].name))

  def possibleMaxScheduledTasks(pr: Seq[PhysicalResource], hr: Seq[HumanResource], t: Time, toSchedule: Seq[ToSchedule],
                       s: Seq[TaskSchedule]): Seq[TaskSchedule] =
    possibleMaxScheduledTasks(pr, hr, t, toSchedule, s, Seq.empty[TaskSchedule])

  @tailrec
  def possibleMaxScheduledTasks(pr: Seq[PhysicalResource], hr: Seq[HumanResource], t: Time, toSchedule: Seq[ToSchedule],
                       s: Seq[TaskSchedule], newS: Seq[TaskSchedule]): Seq[TaskSchedule] =
    if(toSchedule.isEmpty) newS else
      scheduleFirst(pr,hr,toSchedule,s++newS,t) match
        case Some(ts) => possibleMaxScheduledTasks(pr.filterNot(x => ts.physicalResourceIds.contains(x.id)),
          hr.filterNot(x => ts.humanResourceNames.contains(x.name)),
          t, toSchedule.filterNot(x => x == ts),
          s, newS :+ ts)
        case None => newS

  def rescheduleTasks(schedulableTasks : Seq[ToSchedule], newS : Seq[TaskSchedule], s: Seq[TaskSchedule],
                      t: Time, pr : Seq[PhysicalResource], hr : Seq[HumanResource]) : Seq[TaskSchedule] =
    val toReschedule = schedulableTasks.filter(x => newS.exists(ts => x == ts))
      .sortBy(x => (!x.hasBeenStarted(s),x.order.orderId.to,x.productNumber.to))

    toReschedule.foldLeft((toReschedule,Seq.empty[TaskSchedule])){
      case ((head::remainingTasks, s2),st) => {
        optimizedSchedule(
          pr.filterNot(x => s2.exists(ts => ts.physicalResourceIds.contains(x.id))),
          hr.filterNot(x => s2.exists(ts => ts.humanResourceNames.contains(x.name))),
          t,st, remainingTasks.map(_.tasks.head)) match
          case Left(_) => (remainingTasks,s2)
          case Right(ts) => (remainingTasks,s2 :+ ts)
      }
    }._2

  def scheduleMaxTasks(p: Production, s: Seq[TaskSchedule], t: Time,
                       schedulableTasks :Seq[ToSchedule]): Seq[TaskSchedule]=
    if(schedulableTasks.isEmpty) Seq.empty[TaskSchedule] else
      val pr = p.availablePhysicalResources(s, t)
      val hr = p.availableHumanResources(s, t)
      rescheduleTasks(schedulableTasks, possibleMaxScheduledTasks(pr, hr, t, schedulableTasks, s), s, t, pr, hr)

  def schedule(p: Production, toSchedule :Seq[ToSchedule]): Result[Seq[TaskSchedule]] =
    schedule(p, Seq.empty[TaskSchedule], Time.Zero, toSchedule)

  @tailrec
  def schedule(p: Production, s: Seq[TaskSchedule], t: Time,
               toSchedule :Seq[ToSchedule]): Result[Seq[TaskSchedule]] =
    if(toSchedule.isEmpty) Right(s) else
      val newSchedule = s ++ scheduleMaxTasks(p,s, t,toSchedule.filterNot(x => x.isExecuting(s,t)))
      newSchedule.map(_.end).filter(_ > t).sortBy(_.to).headOption match
        case Some(nextT) => schedule(p, newSchedule, nextT, toSchedule.update(newSchedule))
        case None => Left(ImpossibleSchedule)

  def createSchedule(production: Production): Result[Seq[TaskSchedule]] =
    val toSchedule = (for
      order <- production.orders
      product <- production.products.filter(_.id == order.productIdRef)
      quantity <- Quantity.One to order.quantity
    yield ToSchedule(order, quantity, product.processes.map(p => production.tasks.find(t => t.id == p).get))).sortBy(x => (x.order.orderId.to,x.productNumber.to))

    time(schedule(production, toSchedule))
    //schedule(production, toSchedule)
