package domain

import SimpleTypes.*
import scala.xml.Node
import DomainError.*
import xml.XML.*
import domain.Utils.*

final case class HumanResource (id: STHumanId, name: NonEmptyString, handles: Seq[STPhysicalTypeId]) extends Resource:

  def relatesToType(t: STPhysicalTypeId): Boolean =
    this.handles.contains(t)

  def relatesToSingleType: Boolean = handles.size == 1

  override def equals(obj: Any): Boolean =
    obj.asInstanceOf[Matchable] match
      case hr: HumanResource => id == hr.id
      case _ => false

object HumanResource:
  def from(xml: Node): Result[HumanResource] =
    for
      sid <- fromAttribute(xml, "id")
      id <- STHumanId.from(sid)
      sname <- fromAttribute(xml, "name")
      name <- NonEmptyString.from(sname)
      shandles <- traverse((xml \ "Handles"),(n: Node) => fromAttribute(n, "type"))
      handles <- convertSeqResultToResultSeq(shandles.map(STPhysicalTypeId.from(_)))
    yield HumanResource(id, name, handles)
