package domain

import domain.*
import SimpleTypes.*
import DomainError.*
import Utils.*

object OptimizedResourceServiceV2:

  def canSchedule(task: Task, resources: Seq[Resource]): Boolean =
    getResources(task, resources, Seq.empty[Task]) match
      case Right(a) => true
      case Left(b) => false

  private def amountOfBlockedTasks(remainingTasks: Seq[Task], resources: Seq[Resource]): Int =
    remainingTasks.filterNot(t => canSchedule(t, resources)).size

  private def getResourceToAllocate(remainingTasks: Seq[Task], task: Task, resources: Seq[Resource], t: STPhysicalTypeId,
                                                               remainingTypes: Seq[STPhysicalTypeId]): Option[Resource] =
    resources
      .filter(r => r.relatesToType(t))
      .find(r => r.relatesToSingleType) match
      case Some(r) => Some(r)
      case None => resources
        .filter(r => r.relatesToType(t))
        .filter(r => canSchedule(Task(task.id,task.time,remainingTypes),resources.filterNot(x => x.equals(r))))
        .map(r => (r,
          amountOfBlockedTasks(remainingTasks, resources.filterNot(_.equals(r)))))
        .sortBy(x => x._2)
        .map(_._1)
        .headOption

  def getResources(task: Task, resources : Seq[Resource], remainingTasks: Seq[Task]):  Result[Seq[Resource]] =
    convertSeqResultToResultSeq(task.physicalResourceTypes.foldLeft(task.physicalResourceTypes,Seq.empty[(STPhysicalTypeId, Result[Resource])]){
      case ((types, Nil), t) => getResourceToAllocate(remainingTasks,task,resources,t,types.tail)
        .fold((types.tail,Seq((t,Left(ResourceUnavailable(task.id.to, t.to))))))(r => (types.tail,Seq((t,Right(r)))))
      case ((types, ls), t) => getResourceToAllocate(remainingTasks,task,resources.filter(r => !ls.map(_._2).collect { case Right(value) => value}.contains(r)),t,types.tail)
          .fold((types.tail,ls:+(t, Left(ResourceUnavailable(task.id.to, t.to)))))(r => (types.tail,ls:+(t,Right(r))))
    }._2.map(_._2))
