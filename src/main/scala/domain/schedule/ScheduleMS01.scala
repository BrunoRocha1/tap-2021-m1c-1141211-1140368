package domain.schedule

import domain.Utils.convertSeqResultToResultSeq

import scala.xml.*
import domain.*
import xml.XML.*


object ScheduleMS01 extends Schedule:

  // TODO: Create the code to implement a functional domain model for schedule creation
  //       Use the xml.XML code to handle the xml element
  //       Refer to https://github.com/scala/scala-xml/wiki/XML-Processing for xml creation
  def create(xml: Elem): Result[Elem] =
    for
      production <- Production.from(xml)
      schedule <- ScheduleService.createSchedule(production)
    yield TaskSchedule.toXml(schedule)
