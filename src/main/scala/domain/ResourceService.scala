package domain

import domain.*
import SimpleTypes.*
import DomainError.*
import Utils.*

object ResourceService extends ResourceOps:
  def canSchedule(task: Task, resources: Seq[Resource]): Boolean =
    getResources(task, resources) match
      case Right(a) => true
      case Left(b) => false

  private def getResourceToAllocate(remainingTypes: Seq[STPhysicalTypeId])(resources: Seq[Resource], t: STPhysicalTypeId): Option[Resource] =
    resources.filter(r => r.relatesToType(t)).headOption

  def getResources(task: Task, resources : Seq[Resource]):  Result[Seq[Resource]] =
    convertSeqResultToResultSeq(task.physicalResourceTypes.foldLeft(task.physicalResourceTypes,Seq.empty[(STPhysicalTypeId, Result[Resource])]){
      case ((types, Nil), t) => getResourceToAllocate(types)(resources,t)
        .fold((types.tail,Seq((t,Left(ResourceUnavailable(task.id.to, t.to))))))(r => (types.tail,Seq((t,Right(r)))))
      case ((types, ls), t) => getResourceToAllocate(types)(resources.filter(r => !ls.map(_._2).collect { case Right(value) => value}.contains(r)),t)
          .fold((types.tail,ls:+(t, Left(ResourceUnavailable(task.id.to, t.to)))))(r => (types.tail,ls:+(t,Right(r))))
    }._2.map(_._2))
