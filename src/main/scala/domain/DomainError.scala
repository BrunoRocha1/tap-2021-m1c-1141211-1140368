package domain

type Result[A] = Either[DomainError,A]

enum DomainError:
  case IOFileProblem(error: String)
  case XMLError(error: String)
  case EmptyString(error : String)
  case InvalidPhysicalId(error : String)
  case InvalidResource(error : String)
  case InvalidHumanId(error : String)
  case InvalidTaskId(error : String)
  case InvalidProductId(error : String)
  case InvalidOrderId(error : String)
  case InvalidTaskIdRef(error : String)
  case InvalidProductIdRef(error : String)
  case InvalidQuantity(error : String)
  case TaskUsesNonExistentPRT(error: String)
  case ProductDoesNotExist(error: String)
  case TaskDoesNotExist(error: String)
  case ResourceUnavailable(error: String, error2: String)
  case TasksUsesNonExistentHumanResourceHandle(error: String)
  case InvalidPhysicalTypeId(error: String)
  case InvalidTime(error: String)
  case ImpossibleSchedule