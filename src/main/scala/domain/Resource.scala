package domain

import SimpleTypes.*

trait Resource:
  def relatesToType(t: STPhysicalTypeId): Boolean
  def relatesToSingleType: Boolean
  override def equals(obj: Any): Boolean
