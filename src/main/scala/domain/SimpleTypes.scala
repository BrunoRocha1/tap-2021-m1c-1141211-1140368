package domain

import scala.util.Try
import DomainError.*
import scala.annotation.targetName

object SimpleTypes:

  opaque type STPhysicalId = String
  object STPhysicalId:
    def from(s : String) : Result[STPhysicalId] =
      val rx = "PRS_.*".r
      if rx.matches(s) then Right(s) else Left(InvalidPhysicalId(s))
  extension(id:STPhysicalId)
    @targetName("toSTPhysicalId")
    def to: String = id

  opaque type STHumanId = String
  object STHumanId:
    def from(s : String) : Result[STHumanId] =
      val rx = "HRS_.*".r
      if rx.matches(s) then Right(s) else Left(InvalidHumanId(s))
  extension(id:STHumanId)
    @targetName("toSTHumanId")
    def to: String = id

  opaque type STTaskId = String
  object STTaskId:
    def from(s : String) : Result[STTaskId] =
      val rx = "TSK_.*".r
      if rx.matches(s) then Right(s) else Left(InvalidTaskId(s))
  extension(id:STTaskId)
    @targetName("toSTTaskId")
    def to: String = id

  opaque type STProductId = String
  object STProductId:
    def from(s : String) : Result[STProductId] =
      val rx = "PRD_.*".r
      if rx.matches(s) then Right(s) else Left(InvalidProductId(s))
  extension(id:STProductId)
    @targetName("toSTProductId")
    def to: String = id

  opaque type STOrderId = String
  object STOrderId:
    def from(s : String) : Result[STOrderId] =
      val rx = "ORD_.*".r
      if rx.matches(s) then Right(s) else Left(InvalidOrderId(s))
  extension(id:STOrderId)
    @targetName("toSTOrderId")
    def to: String = id

  opaque type NonEmptyString = String
  object NonEmptyString:
    def from(s : String) : Result[NonEmptyString] =
      if !s.trim.isEmpty then Right(s) else Left(EmptyString(s))
  extension(id:NonEmptyString)
    @targetName("toNonEmptyString")
    def to: String = id

  opaque type Quantity = Int
  object Quantity:
    def from (s : String) : Result[Quantity] =
      Try(s.toInt).fold[Result[Quantity]](_ => Left(InvalidQuantity(s)), i => from(i))
    def from (n : Int) : Result[Quantity] =
      if n >= 0 then Right(n) else Left(InvalidQuantity(n.toString))
    def One: Quantity = 1
  extension(q:Quantity)
    @targetName("toQuantity")
    def to: Int = q
    infix def to(q2: Quantity) = (q to q2).map(Quantity.from(_).getOrElse(throw Exception()))

  opaque type Time = Int
  object Time:
    def from (s : String) : Result[Time] =
      Try(s.toInt).fold[Result[Time]](_ => Left(InvalidTime(s)), i => from(i))
    def from (n : Int) : Result[Time] =
      if n >= 0 then Right(n) else Left(InvalidTime(n.toString))
    def Zero: Time = 0
  extension(t: Time)
    @targetName("plusTime")
    infix def +(t2: Time): Time = t + t2

  extension(id:Time)
    @targetName("toTime")
    def to: Int = id
    def <(t2: Time): Boolean = id < t2
    def >(t2: Time): Boolean = id > t2
    def <=(t2: Time): Boolean = id <= t2
    def >=(t2: Time): Boolean = id >= t2

  opaque type STPhysicalTypeId = String
  object STPhysicalTypeId:
    def from(s : String) : Result[STPhysicalTypeId] =
      val rx = "PRST .*".r
      val rx2 = "PRST_.*".r
      if rx.matches(s) || rx2.matches(s) then Right(s) else Left(InvalidPhysicalTypeId(s))
  extension(id:STPhysicalTypeId)
    @targetName("toSTPhysicalTypeId")
    def to: String = id