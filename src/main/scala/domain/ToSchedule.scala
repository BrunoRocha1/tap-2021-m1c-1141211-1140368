package domain

import domain.{Order, Task, TaskSchedule}
import domain.SimpleTypes.{Quantity,Time}

final case class ToSchedule(order: Order, productNumber: Quantity, tasks: Seq[Task]):

  def missingTasks(s: Seq[TaskSchedule]): Seq[Task] =
    tasks.filterNot(t => s.exists(ts => ts.order == order.orderId && ts.productNumber == productNumber && ts.task == t.id))

  def isExecuting(s: Seq[TaskSchedule], t: Time): Boolean =
    s.filter(_.executing(t)).exists(ts => this == ts)

  def hasBeenStarted(s: Seq[TaskSchedule]): Boolean =
    s.exists(ts => this == ts)

  infix def ==(t2: ToSchedule): Boolean =
    order.orderId == t2.order.orderId && productNumber == t2.productNumber

  infix def ==(ts: TaskSchedule): Boolean =
    order.orderId == ts.order && productNumber == ts.productNumber

implicit class ToScheduleSeqExt(val self:Seq[ToSchedule]):

  def update(s: Seq[TaskSchedule]): Seq[ToSchedule] =
    self
      .map(x => ToSchedule(x.order, x.productNumber, x.missingTasks(s)))
      .filterNot(_.tasks.isEmpty)
