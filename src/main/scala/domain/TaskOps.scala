package domain

import domain.*
import domain.SimpleTypes.*

trait TaskOps:
  def scheduleTask(physicalResources: Seq[PhysicalResource])(humanResource: Seq[HumanResource])
                  (orderId: STOrderId, quantity: Int, task: Task, startTime: Int, endTime: Int): Result[TaskSchedule]
