package domain

import domain.SimpleTypes.*
import domain.DomainError.*
import xml.XML.*
import domain.Utils.*

import scala.xml.Node

final case class Task (id: STTaskId, time: Time, physicalResourceTypes: Seq[STPhysicalTypeId])

object Task:
  private def getPhysicalResourceTypes(physicalResources: Seq[PhysicalResource],physicalResourcetypes: Seq[String]): Result[Seq[STPhysicalTypeId]] =
    convertSeqResultToResultSeq(physicalResourcetypes.map(prt =>
      for
        prts <- physicalResources.find(pr => pr.resourceType.to == prt).fold(Left(TaskUsesNonExistentPRT(prt)))(pr => Right(pr.resourceType))
      yield prts
    ))
  def from(physicalResources: Seq[PhysicalResource])(xml: Node): Result[Task] =
    for
      sid <- fromAttribute(xml, "id")
      id <- STTaskId.from(sid)
      stime <- fromAttribute(xml, "time")
      time <- Time.from(stime.toInt)
      sphysicalResourceTypes <- traverse((xml \ "PhysicalResource"),(n: Node) => fromAttribute(n, "type"))
      physicalResourceTypes <- getPhysicalResourceTypes(physicalResources, sphysicalResourceTypes)
    yield Task(id, time, physicalResourceTypes)