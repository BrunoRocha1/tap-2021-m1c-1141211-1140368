package domain

import domain.*
import domain.SimpleTypes.*
import domain.Utils.*
import domain.DomainError.*

object ScheduleService extends ScheduleOps:

  def createSchedule(production: Production): Result[Seq[TaskSchedule]] =
    val toSchedule = for
      order <- production.orders
      product <- production.products.filter(_.id == order.productIdRef)
      quantity <- 1 to order.quantity.to
      process <- product.processes
    yield (production, order, quantity, process)

    toSchedule.foldLeft(Right(Seq.empty[TaskSchedule]).asInstanceOf[Result[Seq[TaskSchedule]]]) {
      case (Right(Nil), (p,o,q,process)) =>
        for
          t <- production.tasks.find(t => t.id == process).fold(Left(TaskDoesNotExist(process.to)))(t => Right(t))
          ts <- TaskService.scheduleTask(p.physicalResources)(p.humanResources)(o.orderId,q,t,0,0+t.time.to)
        yield Seq(ts)
      case (Right(ts:+tail), (p,o,q,process)) =>
        for
          t <- production.tasks.find(t => t.id == process).fold(Left(TaskDoesNotExist(process.to)))(t => Right(t))
          tschedule <- TaskService.scheduleTask(p.physicalResources)(p.humanResources)(o.orderId,q,t,tail.end.to,tail.end.to+t.time.to)
        yield ts:+tail:+tschedule
      case (_1, _2) => _1
    }

