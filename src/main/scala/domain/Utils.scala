package domain

import domain.SimpleTypes.*
import domain.DomainError.*

object Utils:
  def convertSeqResultToResultSeq[A](p: Seq[Result[A]]): Result[Seq[A]] =
    p collectFirst {
      case Left(a) => a
    } toLeft {
      p collect {
        case Right(b) => b
      }
    }

  def getFirstDup[A](s: Seq[A]): Option[A] =
    s.groupBy(identity).collectFirst { case (x,ys) if ys.lengthCompare(1) > 0 => x }

  def time[R](block: => R): R = {
    val t0 = System.currentTimeMillis()
    val result = block
    val t1 = System.currentTimeMillis()
    println("Elapsed time: " + (t1 - t0) + "ms")
    result
  }