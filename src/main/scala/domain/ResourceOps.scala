package domain

import domain.*
import SimpleTypes.*

trait ResourceOps:
  def getResources(task: Task, resources: Seq[Resource]):  Result[Seq[Resource]]
