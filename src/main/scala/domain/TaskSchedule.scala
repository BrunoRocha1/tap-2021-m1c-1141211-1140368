package domain

import domain.SimpleTypes.*
import domain.DomainError.*
import xml.XML.*
import domain.Utils.*
import scala.xml.*

final case class TaskSchedule (order: STOrderId, productNumber: Quantity, task: STTaskId, start: Time, end: Time,
                               physicalResourceIds: Seq[STPhysicalId], humanResourceNames: Seq[NonEmptyString]):

  def overlaps(ts: TaskSchedule): Boolean =
    this.start < ts.end && this.end > ts.start

  def canParallelize(prIds: Seq[STPhysicalId], hrNames: Seq[NonEmptyString]): Boolean =
    !this.physicalResourceIds.exists(prIds.contains) && !this.humanResourceNames.exists(hrNames.contains)

  def executing(t: Time): Boolean = start <= t && end > t

object TaskSchedule:

  def from(order: STOrderId, productNumber: Quantity, task: STTaskId, start: Time, end: Time,
           physicalResourceIds: Seq[STPhysicalId], humanResourceNames: Seq[NonEmptyString]): Result[TaskSchedule] =
    Right(TaskSchedule(order, productNumber, task, start, end, physicalResourceIds, humanResourceNames))

  def taskScheduleXML(ts : TaskSchedule): Elem =
    <TaskSchedule order={ts.order.to} productNumber={ts.productNumber.toString} task={ts.task.to} start={ts.start.toString} end={ts.end.toString}>
      <PhysicalResources>{ts.physicalResourceIds.map(ph => <Physical id={ph.to} />)}</PhysicalResources>
      <HumanResources>{ts.humanResourceNames.map(hr => <Human name={hr.to} />)}</HumanResources>
    </TaskSchedule>

  def toXml(taskScheduLeList: Seq[TaskSchedule]): Elem =
    <Schedule xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../schedule.xsd ">
      {taskScheduLeList.map(x => taskScheduleXML(x))}
    </Schedule>
