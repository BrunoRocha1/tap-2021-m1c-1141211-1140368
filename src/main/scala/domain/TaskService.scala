package domain

import domain.*
import domain.SimpleTypes.*

object TaskService extends TaskOps:

  def scheduleTask(physicalResources: Seq[PhysicalResource])(humanResources: Seq[HumanResource])
                  (orderId: STOrderId, quantity: Int, task: Task, startTime: Int, endTime: Int): Result[TaskSchedule] =
    for
      q <- Quantity.from(quantity)
      st <- Time.from(startTime)
      et <- Time.from(endTime)
      pr <- ResourceService.getResources(task, physicalResources)
      hr <- ResourceService.getResources(task, humanResources)
      task <- TaskSchedule.from(orderId, q, task.id, st, et, pr.map(_.asInstanceOf[PhysicalResource].id), hr.map(_.asInstanceOf[HumanResource].name))
    yield task
