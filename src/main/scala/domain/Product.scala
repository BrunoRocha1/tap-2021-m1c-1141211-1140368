package domain

import SimpleTypes.*
import scala.xml.Node
import DomainError.*
import xml.XML.*
import domain.Utils.*

final case class Product (id : STProductId, name : NonEmptyString, processes : Seq[STTaskId])

object Product:
  private def getProcesses(tasks: Seq[Task], processes: Seq[STTaskId]): Result[Seq[STTaskId]] =
    convertSeqResultToResultSeq(processes.map(p =>
      for
        processes <- tasks.find(t => t.id == p).fold(Left(TaskDoesNotExist(p.to)))(t => Right(p))
      yield processes
    ))
  def from(tasks: Seq[Task])(xml: Node): Result[Product] =
    for
        sid         <-  fromAttribute(xml, "id")
        id          <-  STProductId.from(sid)
        sname       <-  fromAttribute(xml, "name")
        name        <-  NonEmptyString.from(sname)
        ssprocesses  <-  traverse((xml \ "Process"),(n: Node) => fromAttribute(n, "tskref"))
        sprocesses   <-  convertSeqResultToResultSeq(ssprocesses.map(STTaskId.from(_)))
        processes  <-  getProcesses(tasks, sprocesses)
    yield Product(id, name, processes)