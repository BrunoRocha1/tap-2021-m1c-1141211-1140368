package domain

import domain.SimpleTypes.*
import domain.DomainError.*
import xml.XML.fromAttribute
import scala.xml.Node

final case class PhysicalResource (id: STPhysicalId, resourceType: STPhysicalTypeId) extends Resource:

  def relatesToType(t: STPhysicalTypeId): Boolean =
    this.resourceType == t

  def relatesToSingleType: Boolean = true

  override def equals(obj: Any): Boolean =
    obj.asInstanceOf[Matchable] match
      case hr: PhysicalResource => id == hr.id
      case _ => false

object PhysicalResource:
  def from(xml: Node): Result[PhysicalResource] =
    for
      sid <- fromAttribute(xml, "id")
      id <- STPhysicalId.from(sid)
      sresourceType <- fromAttribute(xml, "type")
      resourceType <- STPhysicalTypeId.from(sresourceType)
    yield PhysicalResource(id, resourceType)
