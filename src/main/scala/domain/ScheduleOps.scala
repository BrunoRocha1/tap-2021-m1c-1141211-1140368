package domain

import domain.*

trait ScheduleOps:
  def createSchedule(production: Production): Result[Seq[TaskSchedule]]
