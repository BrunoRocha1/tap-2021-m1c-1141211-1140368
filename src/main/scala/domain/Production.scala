package domain

import domain.SimpleTypes.*
import domain.DomainError.*
import domain.*
import xml.XML.*
import domain.Utils.*
import domain.TaskSchedule

import scala.xml.Node

final case class Production  (physicalResources: Seq[PhysicalResource],
                              tasks: Seq[Task],
                              humanResources: Seq[HumanResource],
                              products: Seq[Product],
                              orders: Seq[Order]):

  def availablePhysicalResources(s: Seq[TaskSchedule], t: Time): Seq[PhysicalResource] =
    physicalResources.filterNot(pr => s.filter(_.executing(t)).exists(ts => ts.physicalResourceIds.contains(pr.id)))

  def availableHumanResources(s: Seq[TaskSchedule], t: Time): Seq[HumanResource] =
    humanResources.filterNot(hr => s.filter(_.executing(t)).exists(ts => ts.humanResourceNames.contains(hr.name)))

object Production:
  def from(xml: Node): Result[Production] =
    for
      physicalResources <- convertSeqResultToResultSeq((xml \ "PhysicalResources" \ "Physical").map(PhysicalResource.from(_)))
      tasks <- convertSeqResultToResultSeq((xml \ "Tasks" \ "Task").map(Task.from(physicalResources)(_)))
      humanResources <- convertSeqResultToResultSeq((xml \ "HumanResources" \ "Human").map(HumanResource.from(_)))
      products <- convertSeqResultToResultSeq((xml \ "Products" \ "Product").map(Product.from(tasks)(_)))
      orders <- convertSeqResultToResultSeq((xml \ "Orders" \ "Order").map(Order.from(products)(_)))
    yield Production(physicalResources, tasks, humanResources, products, orders)
