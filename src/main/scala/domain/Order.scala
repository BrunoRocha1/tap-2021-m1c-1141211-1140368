package domain

import domain.SimpleTypes.*
import domain.DomainError.*
import xml.XML.*
import domain.Utils.*
import scala.xml.Node

final case class Order (orderId : STOrderId, productIdRef : STProductId, quantity : Quantity)

object Order:
  def from(products: Seq[Product])(xml: Node): Result[Order] =
    for
      sorderId        <-  fromAttribute(xml, "id")
      orderId         <-  STOrderId.from(sorderId)
      sproductIdRef   <-  fromAttribute(xml, "prdref")
      productIdRef    <-  STProductId.from(sproductIdRef)
      p               <-  products.find(p => p.id == productIdRef).fold(Left(ProductDoesNotExist(sproductIdRef)))(p => Right(productIdRef))
      squantity       <-  fromAttribute(xml, "quantity")
      quantity        <-  Quantity.from(squantity)
    yield Order(orderId, productIdRef, quantity)

